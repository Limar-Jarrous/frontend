import React from 'react';
import 'tui-image-editor/dist/tui-image-editor.css';
import ImageEditor from '@toast-ui/react-image-editor';
import download from "downloadjs";
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import './imgEditor.css';

const CLOUDINARY_URL = 'https://api.cloudinary.com/v1_1/dfxxp7do4/image/upload';
const CLOUDINARY_UPLOAD_PRESET = 'x7u4wkns';

class ImgEditor extends React.Component {
    state = {
        imgAdded: false,
    }

    imageEditor = React.createRef();

    imageEditorOptions = {
        includeUI: {
            // loadImage: {
            //     path: 'img/sampleImage.jpg',
            //     name: 'SampleImage'
            // },
            // theme: myTheme,
            menu: ["crop", "flip", "rotate", "draw", "shape", "text"].reverse(),
            initMenu: '',
            uiSize: {
                width: '100%',
                height: '100vh'
            },
            menuBarPosition: 'left'
        },
        selectionStyle: {
            cornerSize: 20,
            rotatingPointOffset: 70
        },
        usageStatistics: true
    };


    saveImageHandler = () => {
        const imageEditorInst = this.imageEditor.current.imageEditorInst;
        const data = imageEditorInst.toDataURL();

        if (data) {
            const mimeType = data.split(";")[0];
            const extension = data.split(";")[0].split("/")[1];
            download(data, `image.${extension}`, mimeType);
        }
    }

    uploadImageHandler = (url) => {
        axios.post('http://localhost:8000/memes/upload', {
            imageURL: url,
            uploadedByUserName: `${this.props.userName}`,
            uploadedByUserId: `${this.props.userId}`,
        }).then(result => {
            this.setState({ imgAdded: true });
            console.log(result)
        }).catch(err => console.log(err));
    }

    addMemeHandler = () => {
        const imageEditorInst = this.imageEditor.current.imageEditorInst;
        const data = imageEditorInst.toDataURL();

        if (data) {
            const formData = new FormData();
            formData.append('file', data);
            formData.append('upload_preset', CLOUDINARY_UPLOAD_PRESET);
            // console.log(formData);
            fetch(CLOUDINARY_URL, {
                method: 'POST',
                body: formData,
            })
                .then(response => response.json())
                .then((data) => {
                    if (data.secure_url !== '') {
                        const uploadedFileUrl = data.secure_url;
                        // console.log(uploadedFileUrl);
                        this.uploadImageHandler(uploadedFileUrl)
                    }
                })
                .catch(err => console.error(err));
        }
    }


    render() {

        if (!this.props.isAuth) {
            return <Redirect from='/edit' to='/login' />
        }

        if (this.state.imgAdded) {
            return <Redirect from='/edit' to='/' />
        }

        return (
            <>
                <div className="center">
                    <button className='download-btn' onClick={this.saveImageHandler}>Download</button>
                    <button className='add-btn' onClick={this.addMemeHandler}>Add to memes</button>
                </div>
                <ImageEditor
                    includeUI={this.imageEditorOptions.includeUI}
                    selectionStyle={this.imageEditorOptions.selectionStyle}
                    cssMaxHeight={500}
                    cssMaxWidth={700}
                    ref={this.imageEditor}
                />
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuth: state.auth.token !== null,
        userName: state.auth.userName,
        userId: state.auth.userId,
    }
}

export default connect(mapStateToProps)(ImgEditor);