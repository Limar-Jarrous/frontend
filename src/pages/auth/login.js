import * as actionCreators from '../../redux/actions/auth';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import './box.css';


class Login extends Component {
    state = {
        isSignUp: false,
        email: '',
        password: '',
        userName: '',
    }

    handleSwitchAuthMode = () => {
        this.setState({ isSignUp: !this.state.isSignUp });
    }

    userNameChangeHandler = (e) => {
        this.setState({ userName: e.target.value });
    }

    emailChangeHandler = (e) => {
        this.setState({ email: e.target.value });
    }

    passwordChangeHandler = (e) => {
        this.setState({ password: e.target.value });
    }

    handleSignUp = (e) => {
        e.preventDefault();
        // console.log(this.state)
        this.props.onAuth(this.state.userName, this.state.email, this.state.password, this.state.isSignUp);
    }

    render() {
        if (this.props.isAuth) {
            return <Redirect to='/' />
        }
        const form = (
            !this.state.isSignUp
                ?
                <div className='container '>
                    <form className='box' >
                        <div className='row justify-content-center'>
                            <h1>Sign In</h1>
                        </div>
                        <div className='row justify-content-center'>
                            <div className="form-group col-8 p-0 ">
                                <label htmlFor="exampleInputEmail1">Email address</label>
                                <input type="email" className="form-control" id="exampleInputEmail1"
                                    onChange={this.emailChangeHandler} aria-describedby="emailHelp" placeholder="Enter email" />
                                <small id="emailHelp" className="form-text text-muted"></small>
                            </div>
                        </div>
                        <div className='row justify-content-center'>
                            <div className="form-group col-8 p-0">
                                <label htmlFor="exampleInputPassword1">Password</label>
                                <input type="password" className="form-control" id="exampleInputPassword1"
                                    onChange={this.passwordChangeHandler} placeholder="Password" />
                            </div>
                        </div>
                    </form>
                    <div className='row justify-content-center'>
                        <button className="btn btn-primary mr-5"
                            onClick={this.handleSignUp}
                        >
                            Sign in
                            </button>
                        <button className='btn SwitchModeButton'
                            onClick={this.handleSwitchAuthMode}
                        >
                            Switch to Sign up
                            </button>
                    </div>
                </div>
                :
                <div className='container '>
                    <form className='box' onSubmit={this.onSubmitHandler}>
                        <div className='row justify-content-center'>
                            <h1>Sign Up</h1>
                        </div>
                        <div className='row justify-content-center'>
                            <div className="form-group col-8 p-0">
                                <label htmlFor="exampleInputUsername1">Username</label>
                                <input type="username" className="form-control" id="exampleInputUsername1"
                                    onChange={this.userNameChangeHandler} placeholder="Username" />
                            </div>
                        </div>
                        <div className='row justify-content-center'>
                            <div className="form-group col-8 p-0 ">
                                <label htmlFor="exampleInputEmail1">Email address</label>
                                <input type="email" className="form-control" id="exampleInputEmail1"
                                    onChange={this.emailChangeHandler} aria-describedby="emailHelp" placeholder="Enter email" />
                                <small id="emailHelp" className="form-text text-muted"></small>
                            </div>
                        </div>
                        <div className='row justify-content-center'>
                            <div className="form-group col-8 p-0">
                                <label htmlFor="exampleInputPassword1">Password</label>
                                <input type="password" className="form-control" id="exampleInputPassword1"
                                    onChange={this.passwordChangeHandler} placeholder="Password" />
                            </div>
                        </div>
                    </form>
                    <div className='row justify-content-center'>
                        <button className="btn btn-primary mr-5"
                            onClick={this.handleSignUp}
                        >
                            Sign up
                            </button>
                        <button className='btn SwitchModeButton'
                            onClick={this.handleSwitchAuthMode}
                        >
                            Switch to Sign in
                            </button>
                    </div>
                </div>
        )

        return (
            form
        )
    }
}

const mapStateToProps = state => {
    return {
        isAuth: state.auth.token !== null,
        isLoading: state.auth.isLoading,
        error: state.auth.error,
    };
}

const mapDispatchToProps = dispatch => {
    return {
        onAuth: (userName, email, password, signUp) => { dispatch(actionCreators.auth(userName, email, password, signUp)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);