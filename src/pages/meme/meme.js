import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import * as actionCreators from "../../redux/actions/index";
import "./meme.css";

const Meme = () => {
  const dispatch = useDispatch();
  const params = useParams();
  const memeId = params.id;

  var meme = useSelector((state) => state.memes.memes);

  useEffect(() => {
    dispatch(actionCreators.getMeme(memeId));
  }, [memeId]);

  return (
    <div className="container align-items-center p-0 alig">
      <div className="row justify-content-center">
        <div className="col-8 p-0 h-350">
          <div className="meme-left">
            <div className="meme-img">
              <img src={meme.imageURL} alt="missing" />
            </div>
          </div>
          <div className="meme-right">
            <div className="meme-info">
              <div>Uploaded By: {meme.uploadedByUserName}</div>
              <div>likes: {meme.likes}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Meme;
