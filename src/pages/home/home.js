import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import * as actionCreators from "../../redux/actions/index";
import Card from "../../components/card/card";

const Home = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  var memes = useSelector((state) => state.memes.memes);

  useEffect(() => {
    dispatch(actionCreators.getAllMemes());
    // eslint-disable-next-line
  }, []);

  let memesList = null;
  if (memes.length > 0) {
    memesList = memes.map((meme) => {
      return (
        <Card
          key={meme._id}
          memeId={meme._id}
          imageURL={meme.imageURL}
          likes={meme.likes}
          uploadedByUserName={meme.uploadedByUserName}
        />
      );
    });
  }

  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-12 ">
          <h2 className="text-center m-0">Home Page</h2>
          <br />
        </div>
      </div>
      <div className="row">{memesList}</div>
    </div>
  );
};

export default Home;
