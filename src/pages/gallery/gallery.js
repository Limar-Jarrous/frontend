import React, { useState, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import * as actionCreators from "../../redux/actions/index";
import Card from "../../components/card/card";

const Gallery = () => {
  var memes = useSelector((state) => state.memes.memes);
  var auth = useSelector((state) => state.auth);
  // const [selectedMeme, setSelectedMeme] = useState(null);
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    dispatch(actionCreators.getMemesByUserId(auth.userId));
    // eslint-disable-next-line
  }, []);

  const isAuth = auth.token !== null;
  useEffect(() => {
    if (!isAuth) {
      return history.replace(`/login`);
    }
  }, [isAuth]);

  let memesList = null;
  if (memes.length > 0) {
    memesList = memes.map((meme) => {
      return (
        <Card
          key={meme._id}
          memeId={meme._id}
          imageURL={meme.imageURL}
          likes={meme.likes}
          uploadedByUserName={meme.uploadedByUserName}
        />
      );
    });
  } else {
    memes = <h3>No memes uploaded by this user</h3>;
  }

  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-12 ">
          <h2 className="text-center m-0">Gallery Page</h2>
          <br />
        </div>
      </div>
      <div className="row">{memesList}</div>
    </div>
  );
};

export default Gallery;
