import React, { Component } from 'react';
import * as actionCreators from './redux/actions/index';
import { connect } from 'react-redux';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import './App.css';

import ImageEditor from './pages/imgEditor/imgEditor';
import Gallery from './pages/gallery/gallery';
import Home from './pages/home/home';
import Meme from './pages/meme/meme';
import Navbar from './components/Navbar/Navbar';
import Login from './pages/auth/login';
import Logout from './pages/auth/logout';


class App extends Component {

  componentDidMount() {
    this.props.onTryAutoSignIn();
  }
  render() {

    return (
      <Router>
        <div className="App">
          <Navbar isAuth={this.props.isAuth} />
          <div className="app-content">
            <Switch>
              <Route exact path="/edit" component={ImageEditor} />
              <Route exact path="/gallery" component={Gallery} />
              <Route exact path="/login" component={Login} />
              <Route exact path='/logout' component={Logout} />
              <Route exact path='/meme/:id' component={Meme} />
              <Route exact path="/" component={Home} />
              <Redirect to='page-not-found' />
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuth: state.auth.token !== null,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignIn: () => dispatch(actionCreators.checkAuthState())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
