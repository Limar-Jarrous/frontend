import React from 'react';
import { Link, useHistory } from 'react-router-dom';

const Navbar = (props) => {
    const history = useHistory();
    let path;

    const handleAuthClick = () => {
        path = '/login';
        history.push(path);
    }
    const handleLogoutClick = () => {
        path = '/logout';
        history.push(path);
    }
    return (
        <nav >
            <ul className='nav-list'>
                <li className='nav-item'>
                    {props.isAuth
                        ? <button className='btn-primary' onClick={handleLogoutClick}>Log out</button>
                        : <button className='btn-primary' onClick={handleAuthClick}>Sign up</button>
                    }
                </li>
                <li className='nav-item'>
                    <Link to="/">
                        <i className='nav-icon fa fa-home'></i><br />Home
                    </Link>
                </li>
                <li className='nav-item'>
                    <Link to="/edit">
                        <i className='nav-icon fa fa-edit'></i><br />Edit
                    </Link>
                </li>
                <li className='nav-item'>
                    <Link to="/gallery">
                        <i className='nav-icon fa fa-image'></i><br />Gallery
                    </Link>
                </li>
                {/* <li className='nav-item'>
                    <Link to="/saved">
                        <i className='nav-icon fa fa-star'></i><br />Saved
                    </Link>
                </li> */}
            </ul>
        </nav>
    );
}

export default Navbar;