import React from "react";
import { Link, useHistory } from "react-router-dom";
import "./card.css";

const Card = (props) => {
  const history = useHistory();

  return (
    <>
      <div className="col-12 col-sm-6 col-md-4 col-lg-3 center">
        <div className="card">
          <img
            className="card-img"
            src={props.imageURL}
            alt="missing"
            onClick={() => history.push(`/meme/${props.memeId}`)}
          />
          <div className="card-body">
            <h5 className="card-title">{props.uploadedByUserName}</h5>
            <div className="card-controls">
              <div className="control heart">
                <i className="fa fa-heart"></i>
              </div>
              <div className="control likes">{props.likes}</div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Card;
