import * as actionTypes from "../actions/actionTypes";

const initialState = {
  isLoading: false,
  error: null,
  memes: [],
};

//#region GET Memes By User Id
const getMemesByUserIdStart = (state, action) => {
  return {
    ...state,
    isLoading: true,
    error: null,
    memes: [],
  };
};
const getMemesByUserIdFail = (state, action) => {
  console.log(action);
  return {
    ...state,
    isLoading: false,
    error: action.data,
    memes: [],
  };
};
const getMemesByUserIdSuccess = (state, action) => {
  // console.log(action.memes)
  return {
    ...state,
    isLoading: false,
    error: null,
    memes: action.memes.data,
  };
};
//#endregion

//#region GET All Memes
const getAllMemesStart = (state, action) => {
  return {
    ...state,
    isLoading: true,
    error: null,
    memes: [],
  };
};
const getAllMemesFail = (state, action) => {
  return {
    ...state,
    isLoading: false,
    error: action.data.message,
    memes: [],
  };
};
const getAllMemesSuccess = (state, action) => {
  return {
    ...state,
    isLoading: false,
    error: null,
    memes: action.memes.data,
  };
};
//#endregion

//#region GET Meme
const getMemeStart = (state, action) => {
  return {
    ...state,
    isLoading: true,
    error: null,
    memes: [],
  };
};
const getMemeFail = (state, action) => {
  console.log(action);
  return {
    ...state,
    isLoading: false,
    error: action.data,
    memes: [],
  };
};
const getMemeSuccess = (state, action) => {
  // console.log(action.memes);
  return {
    ...state,
    isLoading: false,
    error: null,
    memes: action.memes.data,
  };
};
//#endregion

const memesReducer = (state = initialState, action) => {
  switch (action.type) {
    // GET All Memes
    case actionTypes.GET_ALL_MEMES_START:
      return getAllMemesStart(state, action);
    case actionTypes.GET_ALL_MEMES_FAIL:
      return getAllMemesFail(state, action);
    case actionTypes.GET_ALL_MEMES_SUCCESS:
      return getAllMemesSuccess(state, action);

    // GET Memes By User Id
    case actionTypes.GET_MEMES_BY_USER_ID_START:
      return getMemesByUserIdStart(state, action);
    case actionTypes.GET_MEMES_BY_USER_ID_FAIL:
      return getMemesByUserIdFail(state, action);
    case actionTypes.GET_MEMES_BY_USER_ID_SUCCESS:
      return getMemesByUserIdSuccess(state, action);

    // GET Memes By its Id
    case actionTypes.GET_MEME_START:
      return getMemeStart(state, action);
    case actionTypes.GET_MEME_FAIL:
      return getMemeFail(state, action);
    case actionTypes.GET_MEME_SUCCESS:
      return getMemeSuccess(state, action);

    default:
      return state;
  }
};

export default memesReducer;
