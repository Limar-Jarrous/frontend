import { combineReducers } from "redux";

import authReducer from "./auth";
import memesReducer from "./memes";

export default combineReducers({
    auth: authReducer,
    memes: memesReducer,
});
