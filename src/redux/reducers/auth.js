import * as actionTypes from "../actions/actionTypes";

const initialState = {
    userId: null,
    token: null,
    error: null,
    isLoading: false,
    userName: null,
    email: null,
};

const authStart = (state, action) => {
    return {
        ...state,
        isLoading: true,
        error: null,
        userId: null,
        token: null,
        userName: null,
        email: null,
    };
};

const authFail = (state, action) => {
    return {
        ...state,
        userId: null,
        token: null,
        isLoading: false,
        error: action.error,
        userName: null,
        email: null,
    };
};

const authSuccess = (state, action) => {
    return {
        ...state,
        userId: action.authData.userId,
        token: action.authData.idToken,
        isLoading: false,
        error: null,
        userName: action.authData.userName,
        email: action.authData.email,
    };
};

const authLogout = (state, action) => {
    return {
        ...state,
        userId: null,
        token: null,
        error: null,
        isLoading: false,
        userName: null,
        email: null,
    };
};

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.AUTH_START: return authStart(state, action);
        case actionTypes.AUTH_SUCCESS: return authSuccess(state, action);
        case actionTypes.AUTH_FAIL: return authFail(state, action);
        case actionTypes.AUTH_LOGOUT: return authLogout(state, action);
        default: return state;
    }
};

export default authReducer;
