import * as actionTypes from "./actionTypes";
import axios from "axios";

//#region GET Memes By User ID
export const getMemesByUserIdStart = () => {
  return {
    type: actionTypes.GET_MEMES_BY_USER_ID_START,
  };
};

export const getMemesByUserIdFail = (error) => {
  return {
    type: actionTypes.GET_MEMES_BY_USER_ID_FAIL,
    error: error,
  };
};

export const getMemesByUserIdSuccess = (memes) => {
  return {
    type: actionTypes.GET_MEMES_BY_USER_ID_SUCCESS,
    memes: memes,
  };
};

export const getMemesByUserId = (userId) => {
  return (dispatch) => {
    dispatch(getMemesByUserIdStart());

    let url = `http://localhost:8000/memes/${userId}`;

    return axios
      .get(url)
      .then((resp) => {
        // console.log(resp)
        dispatch(getMemesByUserIdSuccess(resp));
      })
      .catch((err) => {
        // console.log(err)
        dispatch(getMemesByUserIdFail(err));
      });
  };
};
//#endregion

//#region GET Meme
export const getMemeStart = () => {
  return {
    type: actionTypes.GET_MEME_START,
  };
};

export const getMemeFail = (error) => {
  return {
    type: actionTypes.GET_MEME_FAIL,
    error: error,
  };
};

export const getMemeSuccess = (meme) => {
  return {
    type: actionTypes.GET_MEME_SUCCESS,
    memes: meme,
  };
};

export const getMeme = (memeId) => {
  return (dispatch) => {
    dispatch(getMemeStart());

    let url = `http://localhost:8000/meme/${memeId}`;

    return axios
      .get(url)
      .then((resp) => {
        // console.log(resp)
        dispatch(getMemeSuccess(resp));
      })
      .catch((err) => {
        // console.log(err)
        dispatch(getMemeFail(err));
      });
  };
};
//#endregion

//#region GET All Memes
export const getAllMemesStart = () => {
  return {
    type: actionTypes.GET_ALL_MEMES_START,
  };
};

export const getAllMemesFail = (error) => {
  return {
    type: actionTypes.GET_ALL_MEMES_FAIL,
    error: error,
  };
};

export const getAllMemesSuccess = (memes) => {
  return {
    type: actionTypes.GET_ALL_MEMES_SUCCESS,
    memes: memes,
  };
};

export const getAllMemes = () => {
  return (dispatch) => {
    dispatch(getAllMemesStart());

    let url = `http://localhost:8000/memes/all`;

    return axios
      .get(url)
      .then((resp) => {
        // console.log(resp)
        dispatch(getAllMemesSuccess(resp));
      })
      .catch((err) => {
        // console.log(err)
        dispatch(getAllMemesFail(err));
      });
  };
};
//#endregion
