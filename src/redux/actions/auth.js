import * as actionTypes from './actionTypes';
import axios from 'axios';

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    };
};

export const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    };
};

export const authSuccess = (authData) => {
    // console.log(authData)
    return {
        type: actionTypes.AUTH_SUCCESS,
        authData: authData
    };
};

export const logout = () => {
    localStorage.removeItem("authData");
    return {
        type: actionTypes.AUTH_LOGOUT
    };
};

export const checkAuthTimeout = () => {
    return dispatch => {
        setTimeout(() => {
            dispatch(logout());
        }, 60 * 60 * 1000 /* 1 hour */);
    };
};

export const checkAuthState = () => {
    return dispatch => {
        const jsonAuthData = localStorage.getItem("authData");
        const authData = JSON.parse(jsonAuthData);
        if (!jsonAuthData || authData.idToken === null) {
            dispatch(logout());
        } else {
            dispatch(authSuccess(authData));
        }
    };
};

export const auth = (userName, email, password, signUp) => {
    return (dispatch) => {
        dispatch(authStart());
        const authData = {
            email: email,
            password: password,
            userName: userName
        };
        let url = 'http://localhost:8000/user/login';
        if (signUp) {
            url = 'http://localhost:8000/user/signup';
        }

        return axios
            .post(url, authData)
            .then(resp => {
                // console.log(resp);
                localStorage.setItem("authData", JSON.stringify(resp.data));
                dispatch(authSuccess(resp.data));
                dispatch(checkAuthTimeout());
            }).catch(err => {
                // console.log(err.response.data.message);
                dispatch(authFail(err.response.data.message));
            })
    }
}
